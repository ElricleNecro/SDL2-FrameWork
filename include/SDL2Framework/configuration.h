#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <cstdlib>
#include <filesystem>

class Configuration {
	public:
		static Configuration& get(void);

		void setAppName(const std::string& appname);

		inline const std::filesystem::path& ressource(void) const { return this->_res; };
		inline const std::filesystem::path& configuration(void) const { return this->_conf; };
		inline const std::filesystem::path& log(void) const { return this->_log; };

	public:
		Configuration(Configuration const&) = delete;
		void operator=(Configuration const&)  = delete;

	private:
		Configuration(void) {};
		virtual ~Configuration(void) {};

	private:
		std::string _appname;

		std::filesystem::path _res;
		std::filesystem::path _log;
		std::filesystem::path _conf;
};

#endif /* CONFIGURATION_H */
