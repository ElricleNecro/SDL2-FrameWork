#ifndef EVENTS_H_NASCFPRZ
#define EVENTS_H_NASCFPRZ

#include <tuple>

#include <SDL2/SDL.h>

class Events {
	public:
		Events(const unsigned int width, const unsigned int height);
		virtual ~Events(void);

		void update(void);

		inline uint32_t id(void) const { return this->_winID; };
		inline bool close(void) const { return this->_close; };
		inline bool resized(void) const { return this->_resized; };
		inline const bool* mouse(void) const { return this->_kMouse; };

		inline unsigned int x(void) const { return this->_x; };
		inline unsigned int y(void) const { return this->_y; };

		inline int dx(void) const { return this->_dx; };
		inline int dy(void) const { return this->_dy; };

		inline int wheel_x(void) const { return this->_wx; };
		inline int wheel_y(void) const { return this->_wy; };

		inline const std::tuple<unsigned int, unsigned int>& size(void) const { return this->_windowSize; };

		bool operator[](SDL_Scancode code) const;

	private:
		/* data */
		SDL_Event _evt;

		uint32_t _winID;

		bool _kPress[SDL_NUM_SCANCODES] = { false };
		bool _kMouse[8] = { false };

		unsigned int _x, _y;
		int _dx, _dy;
		int _wx, _wy;

		bool _close = false, _resized = false;

		std::tuple<unsigned int, unsigned int> _windowSize;
};

#endif /* end of include guard: EVENTS_H_NASCFPRZ */
