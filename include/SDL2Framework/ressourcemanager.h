#ifndef RESSOURCEMANAGER_H_5TQJXLIS
#define RESSOURCEMANAGER_H_5TQJXLIS

#include <filesystem>

#include "SDL2Framework/configuration.h"

class RessourceManager {
	public:
		static RessourceManager& get(void);

		inline const std::filesystem::path getShader(const std::string& name) { return this->_root / "shaders" / name; };
		inline const std::filesystem::path getTexture(const std::string& name) { return this->_root / "textures" / name; };
		inline const std::filesystem::path getModel(const std::string& name) { return this->_root / "models" / name; };

	public:
		RessourceManager(RessourceManager const&) = delete;
		void operator=(RessourceManager const&)  = delete;

	private:
		RessourceManager(void) : _root(Configuration::get().ressource()) {};
		virtual ~RessourceManager(void) {};

	private:
		/* data */
		std::filesystem::path _root;
};

#endif /* end of include guard: RESSOURCEMANAGER_H_5TQJXLIS */
