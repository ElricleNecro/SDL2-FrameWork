#ifndef STATE_H
#define STATE_H

#include <cstdint>
#include <SDL2/SDL.h>

#include "SDL2Framework/events.h"

class Window;

class State {
	public:
		State(Window& win) : _win(&win) {};
		virtual ~State(void) = default;

		virtual void handleEvent(Events& e) = 0;
		virtual void update(uint32_t deltatime) {};
		virtual void fixedUpdate(uint32_t deltatime) {};
		virtual void render(SDL_Renderer *renderer) = 0;

	protected:
		Window *_win;
};

#endif /* STATE_H */
