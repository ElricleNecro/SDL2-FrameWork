#ifndef WINDOW_H_DCSIXHIA
#define WINDOW_H_DCSIXHIA

#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

#include <SDL2/SDL.h>
#include <SDL2/SDL_timer.h>

#include "SDL2Framework/events.h"
#include "SDL2Framework/configuration.h"
#include "SDL2Framework/utils/state.h"

class Window {
	public:
		Window(const std::string &title, int width, int height);
		virtual ~Window(void);

		inline bool isClosed(void) const { return _closed; };

		template<typename T, typename... Args>
		void change(Args&&... args);
		template<typename T, typename... Args>
		void push(Args&&... args);
		void push(std::unique_ptr<State> state);
		void pop(void);

		void exit(void);

		void run(void);

	private:
		inline State& currentState(void);

		void handleEvent(void);
		void init(void);

		void tryPop(void);

	private:
		std::string _title;

		Events _evt;

		int _width;
		int _height;

		bool _closed       = false;
		bool _shouldPop    = false;
		bool _shouldChange = false;
		bool _exit         = false;

		SDL_Window   *_window   = nullptr;
		SDL_Renderer *_renderer = nullptr;

		std::vector<std::unique_ptr<State>> _states;
		std::unique_ptr<State> _change;
};

template<typename T, typename... Args>
inline void Window::push(Args&&... args)
{
	push(std::make_unique<T>(std::forward<Args>(args)...));
}

template<typename T, typename ...Args>
inline void Window::change(Args && ...args)
{
	this->_change = std::make_unique<T>(std::forward<Args>(args)...);
	this->_shouldPop = true;
	this->_shouldChange = true;
}

#endif /* end of include guard: WINDOW_H_DCSIXHIA */
