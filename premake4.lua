newoption(
	{
		trigger="install-prefix",
		value="DIR",
		description="Directory used to install bin, lib and share directory.",
	}
)

-- newoption(
	-- {
		-- trigger="with-readline",
		-- description="Use of readline for the prompt.",
	-- }
-- )

-- newoption(
	-- {
		-- trigger="with-readv",
		-- description="Use of process_vm_readv instead of ptrace to read memory.",
	-- }
-- )

-- newoption {
	-- trigger     = "ptrace-use",
	-- value       = "USE",
	-- description = "Choose what to use to read memory with ptrace.",
	-- allowed = {
		-- { "pure",   "Pure ptrace call." },
		-- { "lseek",  "A combination of lseek and read in the mem file (Linux Only)." },
		-- { "pread",  "Use of pread to read the mem file (Linux Only)." }
	-- }
-- }

if not _OPTIONS["install-prefix"] then
	_OPTIONS["install-prefix"] = os.getenv("HOME") .. "/.local/"
end

-- if not _OPTIONS["ptrace-use"] then
	-- _OPTIONS["ptrace-use"] = "pure"
-- end

-- if _OPTIONS["ptrace-use"] == "pure" then
	-- define_ptrace = "USE_PURE_PTRACE"
-- elseif _OPTIONS["ptrace-use"] == "lseek" then
	-- define_ptrace = "USE_lseek_read"
-- else
	-- define_ptrace = "USE_pread"
-- end

solution("Hacking")
	configurations({"debug", "release"})
		buildoptions(
			{
				"-std=c++17"
			}
		)

		flags(
			{
				"ExtraWarnings"
			}
		)

		defines(
			{
				"_GNU_SOURCE"
			}
		)

		includedirs(
			{
				"include/",
			}
		)

	configuration("release")
		buildoptions(
			{
				"-O3", "`sdl2-config --cflags`"
			}
		)

	configuration("debug")
		buildoptions(
			{
				"-g3", "`sdl2-config --cflags`"
			}
		)
		flags(
			{
				"Symbols"
			}
		)

	project("SDL2FrameWork")
		language("C++")
		kind("SharedLib")

		location("build/lib")
		targetdir("build/lib")

		files(
			{
				"src/framework/**"
			}
		)

		linkoptions { "`sdl2-config --libs`", "-lstdc++fs" }

	project("example")
		language("C++")
		kind("ConsoleApp")

		location("build/bin")
		targetdir("build/bin")

		files(
			{
				"src/example/*",
			}
		)

		libdirs(
			{
				"build/lib"
			}
		)

		linkoptions { "-lSDL2FrameWork", "`sdl2-config --libs`", "-lstdc++fs" }
