#include <filesystem>
#include <iostream>

#include "SDL2Framework/events.h"
#include "SDL2Framework/ressourcemanager.h"
#include "SDL2Framework/window.h"

#include "SDL2Framework/utils/state.h"

class BlackScreen : public State {
	public:
		BlackScreen(Window& win);

		// void handleEvent(SDL_Event e) {};
		void handleEvent(Events& evt) override;
		// void update(uint32_t deltatime) override {};
		// void fixedUpdate(uint32_t deltatime) override {};
		void render(SDL_Renderer *renderer) override;
};

BlackScreen::BlackScreen(Window& win) : State(win) {
}

void BlackScreen::handleEvent(Events& evt) {
}

void BlackScreen::render(SDL_Renderer *renderer) {
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
}

void testPath(void) {
	std::cout << RessourceManager::get().getShader("diffrac.fsh") << std::endl;
}

int main(int argc, char *argv[]) {
	Window win("SDL test", 1920, 1080);

	testPath();

	win.push<BlackScreen>(win);
	win.run();

	return 0;
}
