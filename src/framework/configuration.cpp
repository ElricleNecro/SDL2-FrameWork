#include "SDL2Framework/configuration.h"

Configuration& Configuration::get(void) {
	static Configuration instance;

	return instance;
}

void Configuration::setAppName(const std::string& appname) {
	this->_appname = appname;

#ifdef WIN32
	#include <Shlobj.h>  // need to include definitions of constants

	WCHAR path[MAX_PATH];
	if (SUCCEEDED(SHGetKnownFolderPathW(NULL, CSIDL_PROFILE, NULL, 0, path))) {
	  ...
	}

	std::filesystem::path root = path;

	this->_res  = "res";
	this->_conf = root / "Documents" / this->_appname;
	this->_log  = root / "Documents" / this->_appname / (this->_appname + ".log");
#else
	std::filesystem::path root = std::getenv("HOME");

	this->_res  = root / ".local" / "share" / this->_appname;
	this->_conf = root / ".config" / this->_appname;
	this->_log  = root / ".local" / "share" / (this->_appname + ".log");
#endif
}
