#include "SDL2Framework/events.h"

Events::Events(const unsigned int width, const unsigned int height) : _winID(0), _x(0), _y(0), _dx(0), _dy(0), _wx(0), _wy(0), _close(false), _resized(false), _windowSize(width, height) {
}

Events::~Events(void) {
}

void Events::update(void) {
	while( SDL_PollEvent(&this->_evt) ) {
		// Getting the focused windowID:
		this->_winID = this->_evt.window.windowID;

		// Processing the event:
		switch( this->_evt.type ) {
			case SDL_WINDOWEVENT:
				if( this->_evt.window.event == SDL_WINDOWEVENT_CLOSE ) {
					this->_close = true;
				} else if( this->_evt.window.event == SDL_WINDOWEVENT_SHOWN ) {
				} else if( this->_evt.window.event == SDL_WINDOWEVENT_HIDDEN ) {
				} else if( this->_evt.window.event == SDL_WINDOWEVENT_EXPOSED ) {
				} else if( this->_evt.window.event == SDL_WINDOWEVENT_MOVED ) {
				} else if( this->_evt.window.event == SDL_WINDOWEVENT_RESIZED ) {
					this->_resized = true;
					this->_windowSize = std::make_tuple(this->_evt.window.data1, this->_evt.window.data2);
				} else if( this->_evt.window.event == SDL_WINDOWEVENT_SIZE_CHANGED ) {
				} else if( this->_evt.window.event == SDL_WINDOWEVENT_MINIMIZED ) {
				} else if( this->_evt.window.event == SDL_WINDOWEVENT_MAXIMIZED ) {
				} else if( this->_evt.window.event == SDL_WINDOWEVENT_RESTORED ) {
				} else if( this->_evt.window.event == SDL_WINDOWEVENT_ENTER ) {
				} else if( this->_evt.window.event == SDL_WINDOWEVENT_LEAVE ) {
				} else if( this->_evt.window.event == SDL_WINDOWEVENT_FOCUS_GAINED ) {
				} else if( this->_evt.window.event == SDL_WINDOWEVENT_FOCUS_LOST ) {
				} else if( this->_evt.window.event == SDL_WINDOWEVENT_TAKE_FOCUS ) {
				} else if( this->_evt.window.event == SDL_WINDOWEVENT_HIT_TEST ) {
				}
				break;

			case SDL_KEYDOWN:
				this->_kPress[ this->_evt.key.keysym.scancode ] = true;
				break;

			case SDL_KEYUP:
				this->_kPress[ this->_evt.key.keysym.scancode ] = false;
				break;

			case SDL_MOUSEBUTTONDOWN:
				this->_kPress[ this->_evt.button.button ] = true;
				break;

			case SDL_MOUSEBUTTONUP:
				this->_kPress[ this->_evt.button.button ] = false;
				break;

			case SDL_MOUSEMOTION:
				this->_dx = this->_evt.motion.xrel;
				this->_dy = this->_evt.motion.yrel;
				break;

			case SDL_MOUSEWHEEL:
				this->_wx = this->_evt.wheel.x;
				this->_wy = this->_evt.wheel.y;
				break;

			case SDL_QUIT:
				this->_close = true;
				break;
		}

		// Getting cursor position:
		this->_x = this->_evt.motion.x;
		this->_y = this->_evt.motion.y;
	}
}
