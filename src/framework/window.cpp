#include "SDL2Framework/window.h"

Window::Window(const std::string &title, int width, int height) :
	_title(title), _evt(width, height), _width(width), _height(height) {
	this->init();

	// this->push<State>(*this);
}

Window::~Window() {
	SDL_DestroyRenderer(this->_renderer);
	SDL_DestroyWindow(this->_window);

	SDL_Quit();
}

void Window::init() {
	Configuration::get().setAppName(this->_title);

	if( SDL_Init(SDL_INIT_VIDEO) != 0 ) {
		throw std::runtime_error(SDL_GetError());
	}

	this->_window = SDL_CreateWindow(
		this->_title.c_str(),
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		this->_width,
		this->_height,
		SDL_WINDOW_RESIZABLE
	);
	if( this->_window == nullptr ) {
		throw std::runtime_error(SDL_GetError());
	}

	this->_renderer = SDL_CreateRenderer(this->_window, -1, 0);
	if( this->_renderer == nullptr ) {
		throw std::runtime_error(SDL_GetError());
	}
}

void Window::push(std::unique_ptr<State> state) {
	this->_states.push_back(std::move(state));
}

void Window::pop(void) {
	this->_shouldPop = true;
}

void Window::tryPop(void) {
	if( ! this->_shouldPop )
		return ;

	this->_shouldPop = false;

	if( this->_exit ) {
		this->_states.clear();

		return ;
	} else if( this->_shouldChange ) {
		this->_shouldChange = false;
		this->_states.pop_back();
		this->push(std::move(this->_change));

		return ;
	}

	this->_states.pop_back();
}

void Window::exit(void) {
	this->_shouldPop = true;
	this->_exit = true;
}

State& Window::currentState(void) {
	return *this->_states.back();
}

void Window::handleEvent(void) {
	// Getting all events:
	this->_evt.update();

	// Dealing with window only event:
	this->_closed = this->_evt.close();

	// Transmitting remaining events to the current state:
	this->currentState().handleEvent(this->_evt);
}

void Window::run(void) {
	auto time = 0;
	auto last = 0;
	auto lags = 0;

	while( (! this->_closed ) && (! this->_states.empty() ) ) {
		auto& state = this->currentState();

		time = SDL_GetTicks();
		auto elapsed = time - last;
		last = time;
		lags += elapsed;

		this->handleEvent();
		state.update(elapsed);

		SDL_RenderClear(this->_renderer);

		state.render(this->_renderer);

		SDL_RenderPresent(this->_renderer);

		this->tryPop();
	}
}
